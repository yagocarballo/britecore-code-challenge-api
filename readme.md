
# Britecore Challenge API

> Product Development Hiring Project

## Build Setup

Requirements: `Python 3`, `Pyenv`

```bash
## Install all the dependencies
$ pip install -r requirements.txt

## Start the server
$ python ./api.py
```

## Third Party Libraries Used

- [flask](http://flask.pocoo.org/) - This is the Web Framework I used to manage the API creation.
- [SQLAlchemy](https://www.sqlalchemy.org/) - This is the ORM I used to Abstract the Database Implementation from the code.
- [Flask-Restless](https://flask-restless.readthedocs.io/en/stable/) - I used this framework to map SQLAlchemy objects into a Rest API. (this was done for speed of development)
- [Flask-Cors](https://pypi.python.org/pypi/Flask-Cors) - This was used to allow access from JavaScript, to the API which is in a different IP / PORT (this should be more specific for production systems, but is opened to anything here because is just a demo).
- [flask-environ](https://github.com/uniphil/flask-environ) - This was used to read environment variables with configuration. so I could have different settings in my local computer and the server.
- [pymysql](https://github.com/PyMySQL/PyMySQL) - These are the drivers for a MySQL database (MariaDB is used in the demo server)
- [zappa](https://www.zappa.io/) - This is a tool to easily create and deploy the API into a AWS Lambda instance.

## Design Decisions

I made some design decisions on how to build the API and some for Speed of development. 

### Flask Restless

I would normally do the endpoints manually, (I would just declare the HTTP handling in a file), and then I would have a separate class that does not know about HTTP things, and has methods that receive a parameter and return a JSON and a Status Code. This is so these methods can be easily unit tested without involving a server. 

But for the sake of speed, and because I am relatively new in python, I used Flask Restless that creates the REST API for me, so I could focus on the Web Application, where I could do more.

I usually build the APIs of my projects using Go or NodeJS (I have used Python before for Machine Learning and in some projects at University, but never really did an API with python, so this is why I took this Hackathon approach).

### Zappa

I initially started using Zappa as suggested, but the AWS Lambda was having issues talking with the AWS RDS database, even though I set up both in the same subnet, with permissions for the MariaDB port to be accessed from the lambda). I was wasting a bit of time trying to get this communication to work.

So I decided that in the interest of time I could manually deploy to a Digital Ocean Droplet instead.

### Testing

I do not like that I have not added unit tests to this project (Backend or Frontend), I would normally do add at least some simple tests. But I lost a lot of time doing some of the other tasks, and I did not wanted to more than a week to submit this project. I have done some manual testing, but I would not be comfortable saying that tis is a Production ready app, as It probably has bugs that would come out when writing some tests.

## ER Diagram

![DB ER Diagram](./database_er_diagram.png)

## Endpoints

| Endpoint | Methods | Type | Description |
| -------- | ------- | ---- | ----------- |
| `/api/v1/risk` | **GET** | Array<[RiskType](#RiskTypeSchema)> | Gets a list of ALL the Risk Types in the database. |
| `/api/v1/risk/<risk-type>` | **GET**, **POST**, **DELETE**, **PATCH** | [RiskType](#RiskTypeSchema) | Endpoints to Get, Create, Delete and Update a Risk Type. |
| `/api/v1/policy` | **GET** | Array<[Policy](#PolicySchema)> | Gets a list of ALL the Policies in the database. |
| `/api/v1/policy/<policy-id>` | **GET**, **POST**, **DELETE**, **PATCH** | [Policy](#PolicySchema) | Endpoints to Get, Create, Delete and Update a Policy. |

## RiskType Schema

<span id="RiskTypeSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| type | **string** | Name of this risk type. |
| description | **string** | Description of this risk type. |
| fields | **Array<[RiskTypeField](#RiskTypeFieldSchema)>** | List of fields that the form for this Risk Type has. |

## RiskTypeField Schema

<span id="RiskTypeFieldSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| name | **string** | Name of the field. |
| description | **string** | Description of the field. |
| type | **string** | Type of the field. |
| options | **string** | Comma separated options that are used when the field is of type enum. |
| required | **string** | If true, this field is mandatory. |

## Policy Schema

<span id="PolicySchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| firstName | **string** | First name of the customer. |
| secondName | **string** | Middle name of the customer. |
| middleName | **string** | Middle name of the customer. |
| dateOfBirth | **string** | Date of Birth of the customer. |
| risk_type | **string** | Risk Type assigned to this policy. |
| number_fields | **Array<[NumberValue](#NumberValueSchema)>** | List of number values for fields belonging to the Risk Type.  |
| text_fields | **Array<[TextValue](#TextValueSchema)>** | List of text values for fields belonging to the Risk Type.  |
| date_fields | **Array<[DateValue](#DateValueSchema)>** | List of date values for fields belonging to the Risk Type.  |
| enum_fields | **Array<[EnumValue](#EnumValueSchema)>** | List of enum values for fields belonging to the Risk Type.  |

## NumberValue Schema

<span id="NumberValueSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| risk_field_id | **integer** | ID of the Risk Type field this value belongs to. |
| value | **number** | Number value stored here. |

## TextValue Schema

<span id="TextValueSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| risk_field_id | **integer** | ID of the Risk Type field this value belongs to. |
| value | **string** | Text value stored here. |

## DateValue Schema

<span id="DateValueSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| risk_field_id | **integer** | ID of the Risk Type field this value belongs to. |
| value | **date** | Date value stored here. |

## EnumValue Schema

<span id="EnumValueSchema"></span>

| Property | Type | Description |
| -------- | ---- | ----------- |
| risk_field_id | **integer** | ID of the Risk Type field this value belongs to. |
| value | **string** | Enum value stored here. |

