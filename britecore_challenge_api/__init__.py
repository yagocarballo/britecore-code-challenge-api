import sys
import traceback
import flask_restless

from flask import Flask, jsonify
from flask_cors import CORS
from flask_sqlalchemy import *
from flask_environ import get, collect
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

# Create the Flask App
app = Flask(__name__, static_folder='../resources', static_url_path='')

# Add the Cross Origin Request Headers
cors = CORS(app)

# Register the Environment variables so they can be used across the app
app.config.update(collect(
    get('HOST', default='0.0.0.0'),
    get('PORT', default=5000, convert=int),
    get('SQLALCHEMY_DATABASE_URI', default='sqlite:///demo.db')
))

# Create the SQLAlchemy DB engine
engine = create_engine(app.config.get('SQLALCHEMY_DATABASE_URI'))
Session = sessionmaker(bind=engine, autocommit=False, autoflush=False)
s = scoped_session(Session)

Base = declarative_base()
Base.metadata.bind = engine

# Import all models to add them to Base.metadata
from britecore_challenge_api.model import Risk, RiskField

# Create the database if needed using SQLAlchemy
Base.metadata.create_all()

# Create the Flask Restless API Manager
manager = flask_restless.APIManager(app, session=s)

# Import the Flask Restless blueprints to instantiate CRUD Endpoints
from britecore_challenge_api.controller import risk_api_blueprint, policy_api_blueprint

# Register the Flask Restless CRUD Endpoints
app.register_blueprint(risk_api_blueprint)
app.register_blueprint(policy_api_blueprint)


def exception_handler(error):
    """
    Function used to handle unexpected exceptions in the API. (so it returns a JSON error, rather than an HTML Error Page)
    :param error: The error raised by the API Framework.
    :return: The JSON Error reply.
    """
    status_code = getattr(error, 'status_code', 400)
    response_dict = dict(getattr(error, 'payload', None) or ())
    response_dict['error'] = "Unknown Error"
    response_dict['message'] = str(error)
    print(error)

    response = jsonify(response_dict)
    response.status_code = status_code
    traceback.print_exc(file=sys.stdout)
    return response


# Register the Error Handler
app.register_error_handler(Exception, exception_handler)
