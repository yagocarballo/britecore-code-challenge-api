from flask_restless.views import ValidationError

from britecore_challenge_api import manager
from britecore_challenge_api.model import Risk, Policy

# This creates ALL the endpoints for the Risk SQLAlchemy Object
risk_api_blueprint = manager.create_api_blueprint(
    Risk,
    methods=['GET', 'PATCH', 'POST', 'DELETE'],
    url_prefix='/api/v1',
    primary_key='type',
    allow_delete_many=True,
    validation_exceptions=[ValidationError]
)

# This creates ALL the endpoints for the Policy SQLAlchemy Object
policy_api_blueprint = manager.create_api_blueprint(
    Policy,
    methods=['GET', 'PATCH', 'POST', 'DELETE'],
    primary_key='id',
    url_prefix='/api/v1',
    allow_delete_many=True,
    validation_exceptions=[ValidationError],
    include_columns=['id', 'risk_type', 'firstName', 'secondName', 'middleName', 'dateOfBirth'],
    include_methods=['all_fields']
)
