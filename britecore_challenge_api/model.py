import datetime
import enum
import numbers

from flask_restless.views import ValidationError
from sqlalchemy import ForeignKey, Column, Integer, String, Enum, Table, DateTime, Date, Boolean
from sqlalchemy.orm import relationship, validates

from britecore_challenge_api import Base


def getFieldsAsObject(fields_list):
    """
    This function parses a list of field objects into a JSON Serializable Object
    :param fields_list: List of Field Objects
    :return: List of Field objects in a JSON Serializable format.
    """
    all_fields = []
    for field in fields_list:
        all_fields.append({
            'policy_id': field.policy_id,
            'risk_field_id': field.risk_field_id,
            'value': field.value,
        })

    return all_fields


class RiskFieldTypeOptions(enum.Enum):
    """
    Enum with all the available Field Types
    """
    text = 'text'
    number = 'number'
    date = 'date'
    enum = 'enum'


# Table used to link the Risk Type Fields with the Risk Type
risk_fields = Table('risk_fields_association', Base.metadata,
                    Column('risk_id', Integer, ForeignKey('risk.id'), primary_key=True),
                    Column('risk_field_id', Integer, ForeignKey('risk_field.id'), primary_key=True)
                    )


class Risk(Base):
    """
    Class used to represent a Risk Type in the Database.
    """
    __tablename__ = 'risk'
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(String(100), unique=True)
    description = Column(String(255))
    fields = relationship("RiskField", secondary=risk_fields)

    @validates('type')
    def validate_type(self, key, value):
        """
        Function called by Flask Restless for validating the property type
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not 3 <= len(value) <= 100:
            exception = ValidationError()
            exception.errors = dict(type='Type must be between 3 and 100 characters.')
            raise exception
        return value


class RiskField(Base):
    """
    Class used to represent a Risk Type Field in the Database.
    """
    __tablename__ = 'risk_field'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    type = Column(Enum('text', 'number', 'date', 'enum'), nullable=False)
    options = Column(String(255))
    description = Column(String(255))
    required = Column(Boolean())

    @validates('required')
    def validate_value(self, key, value):
        """
        Function called by Flask Restless for validating the property required
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not isinstance(value, bool):
            exception = ValidationError()
            exception.errors = dict(message='Required property must be either true or false.')
            raise exception
        return value


class Policy(Base):
    """
    Class used to represent a Policy in the Database.
    """
    __tablename__ = 'policy'
    id = Column(Integer, primary_key=True)
    risk_type = Column(String(100), ForeignKey("risk.type"))
    firstName = Column(String(255))
    secondName = Column(String(255))
    middleName = Column(String(255))
    dateOfBirth = Column(Date())

    number_fields = relationship("NumberValues")
    text_fields = relationship("TextValues")
    date_fields = relationship("DateValues")
    enum_fields = relationship("EnumValues")

    def all_fields(self):
        """
        Function that returns a list of fields (of different types)
        This is used in Flask Restless to make it more friendly for the Frontend to display the Policy fields.
        :return: The list of all the field values associated with this Policy.
        """
        all = []
        all.extend(getFieldsAsObject(self.number_fields))
        all.extend(getFieldsAsObject(self.text_fields))
        all.extend(getFieldsAsObject(self.date_fields))
        all.extend(getFieldsAsObject(self.enum_fields))
        return all


class NumberValues(Base):
    """
    Class used to represent a number value of a Risk Type Field in the Database.
    """
    __tablename__ = 'number_values'
    policy_id = Column(Integer, ForeignKey("policy.id"), primary_key=True)
    risk_field_id = Column(Integer, ForeignKey("risk_field.id"), primary_key=True)
    value = Column(Integer)

    @validates('value')
    def validate_value(self, key, value):
        """
        Function called by Flask Restless for validating the property value
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not isinstance(value, numbers.Number):
            exception = ValidationError()
            exception.errors = dict(message='Value for \'%s\' must be a Number.' % self.__tablename__)
            raise exception
        return value


class TextValues(Base):
    """
    Class used to represent a text value of a Risk Type Field in the Database.
    """
    __tablename__ = 'text_values'
    policy_id = Column(Integer, ForeignKey("policy.id"), primary_key=True)
    risk_field_id = Column(Integer, ForeignKey("risk_field.id"), primary_key=True)
    value = Column(String(255))

    @validates('value')
    def validate_value(self, key, value):
        """
        Function called by Flask Restless for validating the property value
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not isinstance(value, str):
            exception = ValidationError()
            exception.errors = dict(message='Value for \'%s\' must be a string.' % self.__tablename__)
            raise exception
        return value


class DateValues(Base):
    """
    Class used to represent a date value of a Risk Type Field in the Database.
    """
    __tablename__ = 'date_values'
    policy_id = Column(Integer, ForeignKey("policy.id"), primary_key=True)
    risk_field_id = Column(Integer, ForeignKey("risk_field.id"), primary_key=True)
    value = Column(DateTime())

    @validates('value')
    def validate_value(self, key, value):
        """
        Function called by Flask Restless for validating the property value
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not isinstance(value, datetime.date):
            exception = ValidationError()
            exception.errors = dict(message='Value for \'%s\' must be a valid date.' % self.__tablename__)
            raise exception
        return value


class EnumValues(Base):
    """
    Class used to represent an enum value of a Risk Type Field in the Database.
    """
    __tablename__ = 'enum_values'
    policy_id = Column(Integer, ForeignKey("policy.id"), primary_key=True)
    risk_field_id = Column(Integer, ForeignKey("risk_field.id"), primary_key=True)
    value = Column(String(255))

    @validates('value')
    def validate_value(self, key, value):
        """
        Function called by Flask Restless for validating the property value
        :param key: The name of the property to validate.
        :param value: The value of the property to validate.
        :return: The value (If valid), If the value is not valid, It would raise an exception
        """
        if not isinstance(value, str):
            exception = ValidationError()
            exception.errors = dict(message='Value for \'%s\' must be a string.' % self.__tablename__)
            raise exception
        return value
