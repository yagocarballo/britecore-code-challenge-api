from britecore_challenge_api import app

# Start the Server (if running the server using Python) - Zappa would ignore this point.
if __name__ == '__main__':
    app.run(debug=True, host=app.config.get('HOST'), port=app.config.get('PORT'))
