zappa==0.45.1
flask==0.12.2
SQLAlchemy==1.2.2
flask-sqlalchemy==2.3.2
Flask-Restless==0.17.0
Flask-Cors==3.0.3
flask-environ
pymysql==0.8.0